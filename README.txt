Callpage - Callback for Drupal

-- REQUIREMENTS --
This module requires free account that can be created on https://callpage.io.

-- INSTALLATION --
* Unpack Callpage module in the modules/ folder (currently in the root of your Drupal 8 installation) and enable it in Admin > Extend.
* Then, visit Admin > Structure > Block layout and click Place block (preferably on one of Footer blocks).
* Search for Callpage Widget and confirm with Place block.
* Enter your Callpage Widget Code (which you can find in Dashboard on https://callpage.io/widgets.
* UNCHECK Display Title option.
* Last, check your settings and click Save block.
* Sometimes it is necessary to Save blocks once again on Admin > Structure > Block layout page.

-- CONTACT --
In case of any question or problems feel free to email us at contact@callpage.io.
