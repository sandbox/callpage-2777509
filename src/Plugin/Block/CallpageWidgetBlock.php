<?php

namespace Drupal\callpage\Plugin\Block;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Create Callpage Widget using user's settings.
 *
 * @file CallpageWidgetBlock.php
 * @licence GPLv2 or later
 * @Block(
 *   id = "callpage_widget",
 *   admin_label = @Translation("Callpage Widget"),
 *   category = @Translation("Widgets")
 * )
 */
class CallpageWidgetBlock extends BlockBase {

  /**
   * Build Block with Widget Code.
   */
  public function build() {

    // Retrieve configuration for this block.
    $config = $this->getConfiguration();

    // Check if there is code set up.
    $callpage_enable = isset($config['callpage_enable']) ? $config['callpage_enable'] : 1;
    $callpage_code = isset($config['callpage_code']) ? $config['callpage_code'] : '';

    return [
      '#markup' => $this->t($callpage_enable ? $callpage_code : ''),
    ];
  }

  /**
   * Block configuration form.
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form = parent::blockForm($form, $form_state);

    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();

    // Add a form field to the existing block configuration form.
    $form['callpage_enable'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Callpage Widget'),
      '#default_value' => isset($config['callpage_enable']) ? $config['callpage_enable'] : 1,
    );
    $form['callpage_code'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Widget code:'),
      '#default_value' => isset($config['callpage_code']) ? $config['callpage_code'] : '',
    );

    return $form;
  }

  /**
   * Block configuration form submit handler.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    // Validated code should have CDATA, but Xss::filter has problem with it.
    $pattern = '/<script[^>]*>\/\/ ((.|[\n\r])*)<\/script>/im';
    if (preg_match($pattern, $form_state->getValue('callpage_code'), $matches) === 1) {
      $widget_code = str_replace($matches[1], '', $form_state->getValue('callpage_code'));
      // Prevent XSS.
      $html_tags = ['!--', 'script'];
      $widget_code = Xss::filter($widget_code, $html_tags);
    }
    else {
      drupal_set_message($this->t('Invalid Callpage Widget Code'), 'error');
      return;
    }
    // If everything is all right insert CDATA back.
    $widget_code = str_replace('// ', '// ' . $matches[1], $widget_code);
    // Save our custom settings when the form is submitted.
    $this->setConfigurationValue('callpage_enable', $form_state->getValue('callpage_enable'));
    $this->setConfigurationValue('callpage_code', $widget_code);
  }

  /**
   * Block configuration form validator.
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    parent::blockValidate($form, $form_state);

    $callpage_enable = $form_state->getValue('callpage_enable');
    $callpage_code = $form_state->getValue('callpage_code');

    if (!isset($callpage_enable) || (!is_numeric($callpage_enable) && $callpage_enable > 1)) {
      drupal_set_message($this->t('Callpage Enable must be either checked or unchecked'), 'error');
      $form_state->setErrorByName('callpage_enable', $this->t('Callpage Enable must be either checked or unchecked'));
    }

    if (!isset($callpage_code) || (empty($callpage_code) || strlen($callpage_code) < 10)) {
      drupal_set_message($this->t('Callpage Widget Code is required'), 'error');
      $form_state->setErrorByName('callpage_code', $this->t('Callpage Widget Code is required'));
    }

    if (strpos($callpage_code, "<![CDATA[") === FALSE) {
      drupal_set_message($this->t('Invalid Callpage Widget Code'), 'error');
      $form_state->setErrorByName('callpage_code', $this->t('Invalid Callpage Widget Code'));
    }
  }

}
